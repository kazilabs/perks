from django.contrib import admin
from loyalty.models import Retailer, Store, Customer

class CustomerAdmin(admin.ModelAdmin):
    model = Customer

class RetailerAdmin(admin.ModelAdmin):
    model = Retailer

class StoreAdmin(admin.ModelAdmin):
    model = Store

admin.site.register(Customer, CustomerAdmin)
admin.site.register(Retailer, RetailerAdmin)
admin.site.register(Store, StoreAdmin)
