from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from users.models import User
from django.db import models

class Customer(User):
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    phone_number = models.CharField(max_length=20,null=True,blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    profile_photo = models.ImageField(upload_to='profiles', blank=True)
    total_points = models.FloatField(blank=True, null=True, editable=False)

    class Meta:
        verbose_name = _('Customer')
        verbose_name_plural = _('Customers')

class Retailer(User):
    name = models.CharField(max_length=128,null=True,blank=False)
    phone_number = models.CharField(max_length=20,null=True,blank=True)
    address = models.TextField(max_length=255, blank=True)
    url = models.URLField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    contact_name = models.CharField(max_length=128, null=True, blank=True)

    class Meta:
        verbose_name = _('Retailer')
        verbose_name_plural = _('Retailers')

class Store(models.Model):
    name = models.CharField(max_length=128,null=True,blank=False)
    retailer = models.ForeignKey(Retailer)
    address = models.TextField(max_length=255, blank=False)
    latitude = models.FloatField(blank=False, null=True)
    longitude = models.FloatField(blank=False, null=True, help_text="If you do not enter a latitude and longitude we will try to find them for you using Google Maps.")
    description = models.TextField(blank=True, null=True)
    street = models.CharField(max_length=70)
    city = models.CharField(verbose_name='City/Suburb', max_length=70)
    phone_number = models.CharField(max_length=20,null=True,blank=True)
    url = models.URLField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name

