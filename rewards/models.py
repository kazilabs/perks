from __future__ import unicode_literals

from django.db import models
from loyalty.models import Store, Customer

class Reward(models.Model):
    name = models.CharField(max_length=128, blank=False)
    description = models.TextField(blank=True, null=True)
    display_photo = models.ImageField(upload_to='rewards_dispay', blank=True)
    store = models.ForeignKey(Store, blank=False)
    points = models.FloatField(blank=False, null=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        verbose_name = 'Reward'
        verbose_name_plural = 'Rewards'

class Activity(models.Model):
    name = models.CharField(max_length=128, blank=False)
    description = models.TextField(blank=True, null=True)
    display_photo = models.ImageField(upload_to='rewards_dispay', blank=True)
    points = models.FloatField(blank=False, null=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        verbose_name = 'Activity'
        verbose_name_plural = 'Activities'

class Redeem(models.Model):
    reward = models.ForeignKey(Reward, blank=False)
    customer = models.ForeignKey(Customer, blank=False)
    redeemed_at = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        verbose_name = 'Redeem'
        verbose_name_plural = 'Redeems'
