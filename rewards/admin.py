from django.contrib import admin
from rewards.models import Reward, Redeem, Activity


class ActivityAdmin(admin.ModelAdmin):
    model = Activity

class RewardAdmin(admin.ModelAdmin):
    model = Reward

class RedeemAdmin(admin.ModelAdmin):
    model = Redeem

admin.site.register(Activity, ActivityAdmin)
admin.site.register(Reward, RewardAdmin)
admin.site.register(Redeem, RedeemAdmin)


